#!/usr/bin/make -f

export PYBUILD_NAME=datalad-deprecated

export DATALAD_TESTS_NONETWORK = 1

PY3VERSIONS=$(shell py3versions -vr)

# This is not needed in debhelper 12+ but we are targeting 10

TESTIT = 1
ifneq (,$(filter nocheck,$(subst $(COMMA), ,$(DEB_BUILD_OPTIONS))))
  TESTIT := 0
endif

ifneq (,$(filter nocheck,$(subst $(COMMA), ,$(DEB_BUILD_PROFILES))))
  TESTIT := 0
endif

%:
	dh $@ --buildsystem=pybuild

override_dh_auto_test:
ifeq ($(TESTIT),1)
	# Install special testing directory
	dh_auto_install --destdir $(CURDIR)/debian/install-for-tests
	# Perform the tests
	set -e && \
	        for v in $(PY3VERSIONS); do \
		        moddir=$(CURDIR)/debian/install-for-tests/usr/lib/python$$v/dist-packages/datalad_deprecated; \
			HOME=$(CURDIR)/.pybuild python$$v -m pytest -s -v $$moddir; \
		done
	# Drop the directory afterwards
	rm -rf $(CURDIR)/debian/install-for-tests
endif

override_dh_auto_install:
	PYTHONPATH=. http_proxy='127.0.0.1:9' \
		sphinx-build -N -bhtml docs/source/ \
		debian/tmp/usr/share/doc/datalad-deprecated-doc/html/
	# Work around installing into python3-* package
	dh_auto_install --destdir $(CURDIR)/debian/install-prod
	mv $(CURDIR)/debian/install-prod/* $(CURDIR)/debian/$(PYBUILD_NAME)
	rmdir $(CURDIR)/debian/install-prod
